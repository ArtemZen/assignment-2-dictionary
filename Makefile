.PHONY : clean	

clean : 
	rm *.o
	rm main
	
%.o: %.asm
	nasm -f elf64 $< -o $@

main : lib.o main.o dict.o
	ld -o main lib.o main.o dict.o


