%include "lib.inc"
%include "words.inc"

global _start

extern find_word

section .rodata
	msg_too_long: db 'The string is too long.', 0
	msg_not_found: db 'Not found!', 0
section .text
_start:
	sub rsp, 256			
	mov rdi, rsp				
	mov rsi, 256		
	call read_word			
	cmp rax, 0					
	jz .too_long_err			
	mov rdi, rsp				
	mov rsi, POINTER
	call find_word
	cmp rax, 0
	jz .not_found_err
	mov rdi, rax
	add rdi, 8
	push rdi
	call string_length
	pop rdi
	add rax, rdi
	inc rax
	mov rdi, rax
	call print_string
	xor rdi, rdi
    call exit

	.too_long_err:
		mov rdi, msg_too_long
		jmp .error

	.not_found_err:
		mov rdi, msg_not_found
		jmp .error

	.error:
		call print_err_msg
		call error_exit
