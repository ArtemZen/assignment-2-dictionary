%macro colon 2
	%ifid %2
		%ifdef POINTER
			%%previous: dq POINTER	
		%else
			%%previous: dq 0
		%endif
		%define POINTER %%previous		
	%else
		%fatal 'Второй элемент не ID'
	%endif
	%ifstr %1
		db %1, 0
		%2:
	%else
		%fatal 'Первый элемент не строка'
	%endif
%endmacro
